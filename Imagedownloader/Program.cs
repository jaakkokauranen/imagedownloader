﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imagedownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            FolderManager fm = new FolderManager();
            Downloader downloader = new Downloader();
            fm.MakeFolder();
            Console.WriteLine("Where do you want to download from");
            string url = Console.ReadLine();
            downloader.Download(url, fm.Folder);
            try
            {
                Process.Start(fm.Folder);
            }
            catch (Win32Exception win32Exception)
            {
                // Folder not found
                Console.WriteLine(win32Exception.Message);
            }
        }
    }
}
