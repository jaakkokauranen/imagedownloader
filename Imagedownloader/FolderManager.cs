﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Imagedownloader
{
    class FolderManager
    {
        string folder = "ImageFolder";
        public string Folder
        {
            get { return folder; }
        }

        public bool MakeFolder()
        {
            folder = @"c:\" + folder;
            if(!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
                return true;
            }

            return false;
        }
    }
}
