﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using HtmlAgilityPack;
using System.IO;
using System.Drawing;

namespace Imagedownloader
{
    class Downloader
    {
        public void Download(string urlInput, string folder)
        {
            WebClient client = new WebClient();
            HtmlDocument page = new HtmlDocument();
            string url = client.DownloadString(urlInput);
            page.LoadHtml(url);
            FolderManager fm = new FolderManager();
            List<string> links = new List<string>();

            foreach (HtmlNode node in page.DocumentNode.SelectNodes("//img"))
            {
                links.Add(node.GetAttributeValue("src", string.Empty));
            }

            for (int i = 0; i < links.Count; i++)
            {
                Console.WriteLine(links[i]);
                try
                {
                    client.DownloadFile(new Uri(links[i]), folder + "/image" + i + ".jpg");
                }
                catch
                {
                    continue;
                }
            }
            client.Dispose();
        }
    }
}
